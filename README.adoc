= Generador de examenes

Este proyecto es una plantilla para crear examenes aleatorios usando la infraestructura
de Gitlab, es decir, no necesitas tener instalado ningun software especial, todo se ejecuta
en Gitlab (si así lo deseas). 

Si lo quieres generar/probar en local necesitaras tener instalado:

- groovy version 3.0.5
- asciidoctorj

ambos se pueden instalar de forma cómoda mediante Sdkman


== Instrucciones

Clona el repo en tu cuenta 

La carpeta temas está destinada a contener los enunciados agrupados por tema. Se incluye
un tema "example" como ejemplo.

Cada examen se compone de:

- El enunciado, que se encuentra en un fichero $TEMA.adoc en la carpeta temas
- Las preguntas, que se encuentran en N ficheros llamados como se quiera pero con la extension
adoc en la carpeta $TEMA

Asi pues el examen "example" se compone de los ficheros:

- tema/example.adoc
- tema/example/1.doc
- tema/example/2.adoc
- ...
- tema/example/10.adoc

Se pueden crear tantos temas como se quiera

== Gitlab Pipeline

El proyecto está configurado para que sólo se ejecute el pipeline de forma manual y cuando se proporciona
la variable de entorno TEMA (puedes añadir la opcional MAX_PREGUNTAS para indicar cuantas preguntas incluir en el examen a generar) mediante el interface gráfico de Gitlab

En ese momento se ejecuta un pipeline que ejecutará:

- crear un examen para ese tema con la fecha de hoy
- generar ficheros html con los examenes
- añadir el examen generado al repositorio
- publicar una página web con los examenes disponibles

Una vez que el pipeline se haya ejecutado correctamente tendremos publicados todos los examenes que se hayan
generado gracias a Gitlab Page.

Para ver la url simplemente iremos a la parte de Configuracion, Pages y nos mostrará la url donde están publicados

(La primera vez la publicación suele tardar unos 10 minutos. Los subsiguientes ejecuciones son prácticamente 
inmediatas)


=== Api TOKEN

Para que el pipeline pueda actualizar de forma automática el repo primero hay que crear un API_TOKEN

Desde la página https://gitlab.com/-/profile/personal_access_tokens

crea un token con el nombre que quieras y con el scope `api`. Copia el valor del token generado

A continuación hay que proporcionar al pipeline este valor. Para ello iremos a la configuración de los
pipelines y creamos una variable nueva API_TOKEN con este valor (marcar las dos opciones para proteger
el valor del token y que así no aparezca en los logs)


== Local

Si tienes instalado los requisitos indicados puedes generar los examenes en local

=== Generacion

El generador requiere de un argumento obligatorio TEMA y uno opcional con el maximo de preguntas a
incluir

`groovy generador example 5`

genera un examen para el tema `example` de 5 preguntas aleatorias en el directorio `build` con la fecha
actual  `build/example-2020-11-07.adoc`


=== Conversion

Una vez generado el adoc se convierte a html mediante asciidoctor

