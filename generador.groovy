tema = new File("temas/${args[0]}.adoc")
assert tema.exists()

temaDir = new File("temas/${args[0]}")
assert temaDir.exists()

build = new File("examenes/${args[0]}")
build.mkdirs()

preguntas = temaDir.list()
preguntas.shuffle()

out = new File(build, "${args[0]}-${new Date().format('yyyy-MM-dd-HH-mm')}.adoc")

out.text = tema.text

max = args.length > 1 ? args[1] as int : preguntas.length
preguntas.take(max).each{ p->
   out << new File(temaDir,p).text
}

index = new File("examenes/index.adoc")

index.text = """= Examenes 

Lista de examenes aleatorios organizados por tema

"""

dir = new File("examenes")
dir.eachDir{ d->

   index << "== $d.name \n\n"
   d.eachFile{ f->
      index << "- link:$d.name/${f.name.take(f.name.lastIndexOf('.'))}.html[]"
   }

}